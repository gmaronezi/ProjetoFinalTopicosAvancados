package com.mygdx.game.logic;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.game.MemoryGame;

public class Card extends Actor {

    public float status = -1f;
    public int animated = 0;
    public final static float ROTATE_TIME = 500f;
    private float size;

    private Sprite sprite;
    private Sprite cardBack;
    public String type;

    public boolean isAlive = true;

    public Card(float x, float y, float size, String type, MemoryGame game, final CardController cardController) {
        super();
        this.size = size;
        this.type = type;

        setPosition(x, y);
        setSize(size, size);
        sprite = game.getSprite(type);
        cardBack = game.getSprite("cardBack");

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                cardController.touch(Card.this);
                return true;
            }
        });
    }

    public void startAnimation() {
        if (status == 1)
            //cai aqui quando tiver fazendo a animação de virar a carta para baixo
            animated = -1;
        if (status == -1)
            //cai aqui quando tiver fazendo a animação de virar a carta para cima
            animated = 1;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (status >= 0) {
            //aqui faz a animação da carta quando selecionada
            sprite.setBounds(getX() + size * (1 - status) / 2f, getY(), size * status, size);
            sprite.draw(batch, parentAlpha);
        }
        else {
            float status = -this.status;
            cardBack.setBounds(getX() + size * (1 - status) / 2f, getY(), size * status, size);
            cardBack.draw(batch, parentAlpha);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (animated == 0)
            return;
        if (status >= 1 && animated == 1) {
            //vai cair aqui quando tiver virando as cartas para cima
            animated = 0;
            status = 1;
            return;
        }
        if (status <= -1 && animated == -1) {
            //vai cair aqui quando tiver virando as cartas para baixo
            animated = 0;
            status = -1;
            return;
        }

        //enquanto ta rotacionando ele cai aqui
        status += 2 * animated * (delta * 1000f / ROTATE_TIME);
    }
}
