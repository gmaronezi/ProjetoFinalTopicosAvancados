package com.mygdx.game.logic;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.MemoryGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CardController {

    private MemoryGame game;
    private Stage stage;

    public int countCardColumn, countRow;
    private float x;
    private float y;
    public float cardSize;
    private final float SPACE = 10f;

    private float start_x;
    private float start_y;
    private float cardOff_x_1;
    private float cardOff_y_1;

    public Card[][] field;

    private Card open = null;
    private boolean isWaiting = false;
    public boolean isGameStopped = false;
    public int placar = 0;
    public int tentativas = 0;

    public CardController(MemoryGame game, Stage stage) {
        this.game = game;
        this.stage = stage;
        init();
    }

    private void init() {
        cardSize = 80;
        //quantidade de cartas por linha
        countCardColumn = 4;
        //quantidade de linhas
        countRow = 3;

        float size_x = countCardColumn * cardSize + SPACE * (countCardColumn - 1);
        float size_y = countRow * cardSize + SPACE * (countRow - 1);

        x = game.WIDTH / 2f - size_x / 2f;
        y = game.HEIGHT / 2f - size_y / 2f;

        start_x = game.WIDTH / 2f - cardSize / 2f;
        start_y = game.HEIGHT - cardSize;

        cardOff_x_1 = 0f;
        //esse cálculo pode ser revisto depois
        cardOff_y_1 = game.HEIGHT / 2f - cardSize / 2f - (countCardColumn * countRow / 4f * 5f);

        field = new Card[countRow][countCardColumn];
        generateCards();
    }

    private void generateCards() {
        int cardsCount = countCardColumn * countRow / 2;

        List<String> cards = Arrays.asList(GameUtil.CARDS);
        Collections.shuffle(cards);
        ArrayList<String> cardsSet = new ArrayList<>();
        for (int i = 0; i < cardsCount; ++i) {
            cardsSet.add(cards.get(i));
            cardsSet.add(cards.get(i));
        }
        Collections.shuffle(cardsSet);
        // até aqui basicamente ele recupera a lista de cartas do jogo, embaralhou, e pegou a
        // quantidade que será utilizada no jogo, que no caso é 6

        // verifica se a matriz de cartas do jogo está totalmente limpa, sem nenhuma carta vinculada
        for (int i = 0; i < countRow; ++i) {
            for (int j = 0; j < countCardColumn; ++j) {
                if (field[i][j] != null) field[i][j].remove();
            }
        }

        isGameStopped = false;

        for (int i = 0; i < cardsCount * 2; ++i) {
            String type = cardsSet.get(i);
            int row = i / countCardColumn;
            int column = i % countCardColumn;

            float to_x = this.x + column * (cardSize + SPACE);
            float to_y = this.y + row * (cardSize + SPACE);

            field[row][column] = new Card(start_x, start_y, cardSize, type, game, this);

            stage.addActor(field[row][column]);
            field[row][column].addAction(new SequenceAction(Actions.delay(i * 0.5f / 6f), Actions.moveTo(to_x, to_y, 0.5f, Interpolation.fastSlow)));
        }
    }

    public void touch(final Card card) {
        if (card.status != -1 || !card.isAlive || isWaiting || isGameStopped)
            return;

        //aqui faz a animação de virar a carta
        card.startAnimation();

        if (open == null) {
            open = card;
            return;
        }

        tentativas++;

        //aqui adiciona o efeito e move as cartas para o lado, quando as cartas selecionadas forem iguais
        if (open.type.equals(card.type)) {
            open.isAlive = false;
            card.isAlive = false;
            float to_x = cardOff_x_1;
            float to_y = cardOff_y_1;
            int playerScore = placar;
            card.addAction(new SequenceAction(Actions.delay((Card.ROTATE_TIME + 400f) / 1000f),
                    Actions.moveTo(to_x, to_y + playerScore * 5f, 1f, Interpolation.fastSlow)));
            card.setZIndex(playerScore * 2);
            open.addAction(new SequenceAction(Actions.delay((Card.ROTATE_TIME + 300f) / 1000f),
                    Actions.moveTo(to_x, to_y + playerScore * 5f + 5f, 1f, Interpolation.fastSlow)));
            open.setZIndex(playerScore * 2 + 1);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    placar++;
                }
            }, 1f);
            open = null;
        } else {
            //rotaciona as cartas, quando as duas selecionadas não forem iguais
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    open.startAnimation();
                    card.startAnimation();
                    open = null;
                    isWaiting = false;
                }
            }, (Card.ROTATE_TIME + 600f) / 1000f);
            isWaiting = true;
        }
    }
}
