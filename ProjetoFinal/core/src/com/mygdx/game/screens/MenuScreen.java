package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.game.MemoryGame;
import com.mygdx.game.logic.Button;

public class MenuScreen extends InputAdapter implements Screen {

    private MemoryGame game;
    private Stage stage;
    private StretchViewport viewport;
    private SpriteBatch batch;

    public MenuScreen(MemoryGame game) {
        this.game = game;

        batch = new SpriteBatch();
        viewport = new StretchViewport(game.WIDTH, game.HEIGHT);
        stage = new Stage(viewport, batch);
        Gdx.input.setInputProcessor(stage);

        init();
    }

    private void init() {
        Button play = new Button("single1", "single2", "single3", (game.WIDTH / 2) - (75 / 2) , 205, 75, 75, game);
        play.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                game.setScreen(new GameScreen(game));
            }
        });
        stage.addActor(play);

        Button quit = new Button("quit1", "quit2", "quit3", 0f, 0f, 50f, 50f, game);
        quit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                Gdx.app.exit();
            }
        });
        stage.addActor(quit);

        Label memory = new Label("Jogo da Memória", game.skin);
        memory.setPosition(game.WIDTH / 2f - memory.getWidth() / 2f, game.HEIGHT - memory.getHeight() - 10);
        stage.addActor(memory);

        Label best = new Label("O melhor placar é " +
                Gdx.app.getPreferences("Memory Game").getInteger("bestScore", 0), game.skin, "small");
        best.setPosition(game.WIDTH / 2f - best.getWidth() / 2f,
                game.HEIGHT - memory.getHeight() - best.getHeight() - 50f);
        stage.addActor(best);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(game.background, 0, 0, game.WIDTH, game.HEIGHT);
        batch.end();

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
    }

}
