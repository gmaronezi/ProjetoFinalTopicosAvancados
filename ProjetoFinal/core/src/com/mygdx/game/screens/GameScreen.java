package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mygdx.game.MemoryGame;
import com.mygdx.game.logic.Button;
import com.mygdx.game.logic.CardController;

class GameScreen implements Screen {

    private MemoryGame game;

    private Stage stage;
    private StretchViewport viewport;
    private SpriteBatch batch;

    private CardController cardController;
    private int finalScore;

    private BitmapFont font = new BitmapFont(Gdx.files.internal("fonts/font_normal.fnt"));
    private BitmapFont big_font = new BitmapFont(Gdx.files.internal("fonts/font_big.fnt"));
    private boolean isGameEnded = false;

    public GameScreen(MemoryGame game) {
        this.game = game;

        batch = new SpriteBatch();
        viewport = new StretchViewport(game.WIDTH, game.HEIGHT);
        stage = new Stage(viewport, batch);

        font.getData().setScale(0.3f);
        big_font.getData().setScale(0.7f);

        initUI();
    }

    private void initUI() {
        Button back = new Button("back1", "back2", "back3", 0f,
                game.HEIGHT - 50f, 50f, 50f, game);
        back.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                dispose();
                game.setScreen(new MenuScreen(game));
            }
        });
        stage.addActor(back);
        cardController = new CardController(game, stage);
    }


    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 255f, 204f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        batch.begin();
        font.draw(batch, "Tentativas - " + cardController.tentativas, 650f, game.HEIGHT - 20f);
        font.draw(batch, "" + cardController.placar,
                cardController.cardSize / 2f, - 10f);

        if (cardController.placar * 2 == cardController.countCardColumn * cardController.countRow && !isGameEnded) {
            isGameEnded = true;

            float c = 1f;
            finalScore = (int) Math.floor(100f * cardController.countCardColumn * cardController.countRow / cardController.tentativas * c / 2f);

            Preferences pref = Gdx.app.getPreferences("Memory Game");
            int best = pref.getInteger("bestScore", 0);
            if (finalScore > best)
                pref.putInteger("bestScore", finalScore);
            pref.flush();
        }

        if (isGameEnded) {
            big_font.draw(batch, "Seu placar é " + finalScore, 140f, 250f);
        }

        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        font.dispose();
        stage.dispose();
        batch.dispose();
    }

}
