package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.screens.*;

public class MemoryGame extends Game {

	public TextureAtlas atlas;
	public Texture background;
	public Skin skin;

	public final int WIDTH = 800;
	public final int HEIGHT = 450;

	@Override
	public void create () {
		load();
		this.setScreen(new MenuScreen(this));
	}

	private void load() {
		atlas = new TextureAtlas(Gdx.files.internal("assets_out/myAssets.atlas"));
		background = new Texture(Gdx.files.internal("background.jpg"));
		skin = new Skin(Gdx.files.internal("skin/skin.json"));
	}

	public Sprite getSprite(String name) {
		return atlas.createSprite(name);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {

	}
}
